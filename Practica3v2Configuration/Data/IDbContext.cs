﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practica4.Data.Models;

namespace UPB.Practica4.Data
{
    public interface IDbContext
    {
       
        

        //Tomar en cuenta que debo completar para la practica 4

        Group AddGroup(Group group);
        Group UpdateGroup(Group groupToUpdate);
        Group DeleteGroup(Group groupToDelete);
        List<Group> GetAll();



    }



}


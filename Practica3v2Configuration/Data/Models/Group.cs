﻿using System;

namespace UPB.Practica4.Data.Models
{
    public class Group
    {
        //DAO DAta access object
        public String Id { get; set; }

        public String Name { get; set; }
        public int AvailableSlots { get; set; }


    }
}

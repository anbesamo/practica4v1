﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practica4.Data.Models;

namespace UPB.Practica4.Data
{
    public class DbContext : IDbContext
    {
        //access to db server
        //reference to DB tables (entities)
        public List<Group> GroupTable { get; set; }
        Random r = new Random();

        public DbContext() 
        {
            //Read from config file the DB Connection String
            //OrM for net core is (entity framework  core)
            GroupTable = new List<Group>();
            List<string> names = new List<string>() { "Amarillo","Verde","Jamy","Gris","Negro","Suave","Duro" };
            for (int i = 1; i < 20; i++)
            {
                
                Group p = new Group();
                p.Id = $"Group-{i}";
                p.AvailableSlots = i + 5;
                p.Name=(names[r.Next(0,7)]);

                GroupTable.Add(p);
                

            }



        }

        //Tomar en cuenta que debo completar para la practica 4

        public Group AddGroup(Group group) 
        {
            GroupTable.Add(group);
            return group;
        }
        public Group UpdateGroup(Group groupToUpdate)
        {
            //select * from person where name = "person name"
            Group foundgroup = GroupTable.Find(group=> group.Id == groupToUpdate.Id);
            //foundgroup.Id = groupToUpdate.Id;
            foundgroup.Name =groupToUpdate.Name;
            foundgroup.AvailableSlots = groupToUpdate.AvailableSlots;

            return foundgroup;
        }
        public Group DeleteGroup(Group groupToDelete)
        {
            //PersonTable.RemoveAll(person => person.CI === personTODelete.CI)
            //GroupTable.Remove(groupToDelete);
            GroupTable.RemoveAll(group => group.Id == groupToDelete.Id);

            return groupToDelete;
        }
        public List<Group> GetAll() 
        {

            return GroupTable;
        }


        
    }
}

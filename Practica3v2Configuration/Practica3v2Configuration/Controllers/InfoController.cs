﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Practica3v2Configuration.Controllers
{
    [Route("api/info")]
    [ApiController]
    
    public class InfoController : ControllerBase
    {

        private readonly IConfiguration _config;

        public InfoController(IConfiguration config)
        {
            _config = config;
        }
        [HttpGet]
        public string GetInfo() {
            string projectTitle = _config.GetSection("Project").GetSection("Title").Value;
            string environmentName = _config.GetSection("EnvironmentName").Value;
            string dbConnection = _config.GetConnectionString("DataSource");

            Console.Out.WriteLine($"Info connecting to :{dbConnection}");

            string res = "Project Title:" + projectTitle + "\nEnvironment Name:" + environmentName;
            return res;
        }

    }
}

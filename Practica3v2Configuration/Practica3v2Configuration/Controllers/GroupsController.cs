﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UPB.Practica4.Logic.Models;
using UPB.Practica4.Logic;
using UPB.Practica4.Logic.Managers;
namespace Practica3v2Configuration.Controllers
{
    [ApiController]
    [Route("/api/groups")]
    public class GroupsController : ControllerBase
    {
        Random r = new Random();

        private readonly IConfiguration _config;

        private readonly IGroupManager _groupManager;
        public GroupsController(IConfiguration config, IGroupManager groupManager)
        {
            _config = config;
            _groupManager = groupManager;
        }

       

        [HttpGet]
        public List<Group> GetGroup()
        {
            return _groupManager.GetAllGroups();
        }

        [HttpPost]
        //public Person CreatePerson([FromBody] string pname,[FromBody] string lname,[FromBody] int age)
        public Group CreateGroup([FromBody] Group p)
        {
            return _groupManager.CreateGroup(p);

        }


        [HttpPut]
        public Group UpdateGroup([FromBody] Group p)
        {
           return  _groupManager.UpdateGroup(p);
        }

        [HttpDelete]
        public Group DeleteGroup([FromBody] Group p)
        {
            return _groupManager.DeleteGroup(p);
        }

    }
}

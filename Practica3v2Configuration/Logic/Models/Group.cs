using System;

namespace UPB.Practica4.Logic.Models
{
    public class Group
    {
        // dto  database transfere object
        public String Id { get; set; }

        public String Name { get; set; }
        public int AvailableSlots { get; set; }
    }
}

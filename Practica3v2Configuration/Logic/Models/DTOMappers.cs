﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UPB.Practica4.Logic.Models
{
    //no crear clases, por eso static
    public static class DTOMappers
    {
        public static List<Group> MapGroups(List<Data.Models.Group> groups)
        {
            List<Group> mappedGroups = new List<Group>();
            foreach (Data.Models.Group group in groups)
            {
                mappedGroups.Add(new Group()
                {
                    Id = group.Id,
                    Name = group.Name,
                    AvailableSlots = group.AvailableSlots
                }) ;
            
            }
            return mappedGroups;
        }
        public static Group changeGroupsLogic(Data.Models.Group group)
        {
            Group newGroup = new Group();

            newGroup.Id = group.Id;
            newGroup.Name= group.Name;
            newGroup.AvailableSlots = group.AvailableSlots;
           


            return newGroup;
        }

        public static Data.Models.Group changeGroupsData(Group group)
        {
            Data.Models.Group newGroup = new Data.Models.Group();

            newGroup.Id = group.Id;
            newGroup.Name = group.Name;
            newGroup.AvailableSlots = group.AvailableSlots;



            return newGroup;
        }
        /*
        public static List<Group> MapGroups2(List<Logic.Models.Group> groups)
        {
            List<Group> mappedGroups = new List<Group>();
            foreach (Logic.Models.Group group in groups)
            {
                mappedGroups.Add(new Group()
                {
                    Id = group.Id,
                    Name = group.Name,
                    AvailableSlots = group.AvailableSlots
                });

            }
            return mappedGroups;
        }
        */





    }
}

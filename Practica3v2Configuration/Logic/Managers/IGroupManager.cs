﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practica4.Logic.Models;

namespace UPB.Practica4.Logic
{
    public interface IGroupManager
    {

        List<Group> GetAllGroups();

        public Group DeleteGroup(Group p);

        public Group CreateGroup(Group p);
        public Group UpdateGroup(Group p);

    }
}

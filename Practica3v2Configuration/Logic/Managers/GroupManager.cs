﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practica4.Logic.Models;
using UPB.Practica4.Logic.Managers;
using UPB.Practica4.Data;


namespace UPB.Practica4.Logic.Managers
{
    public class GroupManager:IGroupManager
    {
        //Random r = new Random();

       

        private readonly IDbContext _dbContext;

        public GroupManager(IDbContext dbcontext) 
        {
            _dbContext = dbcontext;
        }
        public List<Group> GetAllGroups()
        {

            List<Data.Models.Group> groups = _dbContext.GetAll();
            return DTOMappers.MapGroups(groups);

        }


        public Group DeleteGroup(Group p)
        {
            /*
            List<Group> gr = this.GetAllGroups();
            if (!gr.Contains(p))
            {
                throw new Exception();
            }
            */
            Data.Models.Group g = _dbContext.DeleteGroup(DTOMappers.changeGroupsData(p));


            return DTOMappers.changeGroupsLogic(g);

        }

        public Group CreateGroup(Group p)
        {
            if (String.IsNullOrEmpty(p.Name) && p.Name.Length>50)
            {

                //capa logica es validar los datos
                throw new Exception();

            }
            if (p.AvailableSlots < 1) 
            {
                throw new Exception();
            }

            //ver si el Name esta bien escirto sino avisar
            string[] partId = p.Id.Split("-");

            //confirmar que son dos elementos y no haya errores tipo group-1-89
            int l = partId.Length;
            int i = this.GetAllGroups().Count + 1;

            if (!(l<3 &&  partId[0] == "Group" && Int32.Parse(partId[1])==i  )) 
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Out.WriteLine($"Numero de id es incorrecto, lo cambiaremos..");
                Console.ForegroundColor = ConsoleColor.White;
                p.Id = $"Group-{i}";

            }
            
          

            Data.Models.Group g = _dbContext.AddGroup(DTOMappers.changeGroupsData(p));
            return DTOMappers.changeGroupsLogic(g);
            

        }

        public Group UpdateGroup(Group p)
        {
            if (String.IsNullOrEmpty(p.Name) && p.Name.Length > 50)
            {

                //capa logica es validar los datos
                throw new Exception();

            }
            if (p.AvailableSlots < 1)
            {
                throw new Exception();
            }


            Data.Models.Group g = _dbContext.UpdateGroup(DTOMappers.changeGroupsData(p));

          
            return DTOMappers.changeGroupsLogic(g);
        }



    }

    

}
